package com.hendisantika.springbootkotlinthymeleaf.controller

import com.hendisantika.springbootkotlinthymeleaf.domain.Student
import com.hendisantika.springbootkotlinthymeleaf.form.CreateStudentForm
import com.hendisantika.springbootkotlinthymeleaf.service.StudentService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-kotlin-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 08/04/20
 * Time: 08.04
 */
@Controller
@RequestMapping("/students")
class StudentController {

    @Autowired
    lateinit var studentService: StudentService

    @GetMapping
    fun listStudents(model: Model): String {
        model.addAttribute("students", studentService.getStudents())
        return "students"
    }

    @GetMapping("/{studentId}")
    fun findStudent(@PathVariable("studentId") sId: String, model: Model): String {
        model.addAttribute("student", studentService.findStudentById(sId))
        return "student"
    }

    @RequestMapping(method = arrayOf(RequestMethod.POST))
    fun addStudent(createStudentForm: CreateStudentForm, model: Model): String {

        studentService.createStudent(Student(
                studentId = createStudentForm.studentId!!,
                lastName = createStudentForm.lastName!!,
                firstName = createStudentForm.firstName!!))

        return "redirect:/students/" + createStudentForm.studentId
    }

    @GetMapping("/add")
    fun createStudentPage(model: Model): String {
        model.addAttribute("studentForm", CreateStudentForm())
        return "new-student-form"
    }
}