package com.hendisantika.springbootkotlinthymeleaf.form

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-kotlin-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 08/04/20
 * Time: 07.54
 */
class CreateStudentForm {
    var studentId: String? = null
    var lastName: String? = null
    var firstName: String? = null
}