package com.hendisantika.springbootkotlinthymeleaf.service

import com.hendisantika.springbootkotlinthymeleaf.domain.Student
import org.springframework.stereotype.Service

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-kotlin-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 08/04/20
 * Time: 07.55
 */
@Service
class StudentService {

    // This represents your database or any data source
    val studentList: MutableList<Student> = mutableListOf(
            Student(studentId = "1", lastName = "Naruto", firstName = "Uzumaki"),
            Student(studentId = "2", lastName = "Sasuke", firstName = "Uchiha"),
            Student(studentId = "3", lastName = "Sasuke", firstName = "Uchiha"),
            Student(studentId = "4", lastName = "Kakashi", firstName = "Hatake"),
            Student(studentId = "5", lastName = "Lee", firstName = "Rock"),
            Student(studentId = "6", lastName = "Ten-ten", firstName = "Ten-ten"),
            Student(studentId = "7", lastName = "Neji", firstName = "Hyuga"),
            Student(studentId = "8", lastName = "Guy", firstName = "Maito"),
            Student(studentId = "9", lastName = "Kurenai", firstName = "Yuuha"),
            Student(studentId = "10", lastName = "Shino", firstName = "Aburame"),
            Student(studentId = "11", lastName = "Kiba", firstName = "Inuzuka"),
            Student(studentId = "12", lastName = "Hinata", firstName = "Hyuga"),
            Student(studentId = "13", lastName = "Asuma", firstName = "Sarutobi"),
            Student(studentId = "14", lastName = "Shikamaru", firstName = "Naara"),
            Student(studentId = "15", lastName = "Ino", firstName = "Yamanaka"),
            Student(studentId = "16", lastName = "Choji", firstName = "Akamichi"),
            Student(studentId = "17", lastName = "Sakura", firstName = "Haruno"))

    fun getStudents(): List<Student> {
        return studentList
    }

    fun findStudentById(id: String): Student? {
        return studentList.find { s -> s.studentId.equals(id) }
    }

    fun createStudent(student: Student) {
        studentList.add(student)
    }

}