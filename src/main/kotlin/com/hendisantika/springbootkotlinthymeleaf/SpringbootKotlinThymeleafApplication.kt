package com.hendisantika.springbootkotlinthymeleaf

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.thymeleaf.dialect.IDialect
import org.thymeleaf.extras.java8time.dialect.Java8TimeDialect


@SpringBootApplication
class SpringbootKotlinThymeleafApplication

@Bean
fun conditionalCommentDialect(): IDialect? {
    return Java8TimeDialect()
}

fun main(args: Array<String>) {
    runApplication<SpringbootKotlinThymeleafApplication>(*args)
}
