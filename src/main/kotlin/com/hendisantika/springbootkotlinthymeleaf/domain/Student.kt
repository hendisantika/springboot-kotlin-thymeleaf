package com.hendisantika.springbootkotlinthymeleaf.domain

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-kotlin-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 08/04/20
 * Time: 07.54
 */
class Student(val studentId: String, val lastName: String, val firstName: String)