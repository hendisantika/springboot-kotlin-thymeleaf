module.exports = {
    platform: ['gitlab', 'maven']
    hostRules: [
        {
            hostType: 'github',
            matchHost: 'github.com',
            token: process.env.GITHUB_TOKEN, // Use environment variable for GitHub token
        },
        {
            hostType: 'gitlab',
            matchHost: 'gitlab.com',
            token: process.env.GITLAB_TOKEN, // Use environment variable for GitLab token
        }
    ],
    endpoint: process.env.CI_API_V4_URL,
    token: process.env.GITLAB_TOKEN,
    gitAuthor: 'Renovate Bot <hendisantika@yahoo.co.id>',
    labels: ['renovate', 'dependencies', 'automated'],
    assignees: ['hendisantika'],
    onboarding: true,
    onboardingConfig: {
        extends: ['config:recommended'],
    },
    repositories: [process.env.CI_PROJECT_PATH]
};


