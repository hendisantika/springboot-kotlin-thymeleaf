# Spring Boot Kotlin Thymeleaf
## Things todo list:
1. Clone this repository: `git clone https://gitlab.com/hendisantika/springboot-kotlin-thymeleaf.git`.
2. Go to folder: `cd springboot-kotlin-thymeleaf`.
3. Run the application: `gradle clean bootRun`
4. Open your favorite browser: http://localhost:8080
4. Heroku Link: https://kotlin-thymeleaf.herokuapp.com

## Screen shot

Index Page

![Index Page](img/index.png "Index Page")

Add New Student

![Add New Student](img/add.png "Add New Student")

List All Students

![List All Students](img/list.png "List All Students")


List All Students on Heroku

![List All Students on Heroku](img/heroku.png "List All Students on Heroku")
